//
//  RegisterViewController.swift
//  SignIn
//
//  Created by Mojave on 7/26/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    
    
    @IBOutlet weak var viewInputName: UIView!
    @IBOutlet weak var viewInputPhoneNumber: UIView!
    @IBOutlet weak var viewInputEmail: UIView!
    
    var textfieldName = SkyFloatingLabelTextField()
    var textFieldEmail = SkyFloatingLabelTextField()
    var textFieldPassword = SkyFloatingLabelTextField()
    
    private var isSelectRegisterOwnEmail: Bool = false
    //private var userRegister: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupInputTextField()
        
    }
    
    
    func setupInputTextField() {
        
        //Set up for textfield Name
        textfieldName = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: viewInputName.frame.width, height: viewInputName.frame.height/2))
        textfieldName.placeholder = "Name"
        textfieldName.title = "Name"
        textfieldName.selectedTitleColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha: 1.0)
        textfieldName.lineErrorColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha: 1.0)
        self.viewInputName.addSubview(textfieldName)
        
        //Set up for textfield Phone Number
        textFieldEmail = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: viewInputPhoneNumber.frame.width, height: viewInputPhoneNumber.frame.height/2))
        textFieldEmail.placeholder = "Email address"
        textFieldEmail.title = "Email address"
        textFieldEmail.selectedTitleColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha: 1.0)
        textFieldEmail.lineErrorColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha: 1.0)
        textFieldEmail.keyboardType = .emailAddress
        self.viewInputPhoneNumber.addSubview(textFieldEmail)
        
        //Set up for textfield Email
        textFieldPassword = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: viewInputEmail.frame.width, height: viewInputEmail.frame.height/2))
        textFieldPassword.placeholder = "Password"
        textFieldPassword.title = "Password"
        textFieldPassword.keyboardType = .default
        textFieldPassword.isSecureTextEntry = true
        textFieldPassword.selectedTitleColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha: 1.0)
        textFieldPassword.lineErrorColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha: 1.0)
        self.viewInputEmail.addSubview(textFieldPassword)
        
    }
    
    
    //Action
    
    
    @IBAction func buttonRegisterOwnEmailPressed(_ sender: Any) {
        
        if textfieldName.text == "" {
            textfieldName.errorMessage = "Please enter name"
        } else if textFieldEmail.text == ""{
            textFieldEmail.errorMessage = "Please enter email address"
        } else if textFieldPassword.text == "" {
            textFieldPassword.errorMessage = "Please enter password"
        }
            
            // check the account is already in the server or not
        else  {
            Auth.auth().createUser(withEmail: textFieldEmail.text!, password: textFieldPassword.text!) { authResult, error in
                
                if let error = error{
                    print(error.localizedDescription)
                    let alertRegister = UIAlertController(title: "Oops", message: "Email has been used", preferredStyle: .alert)
                    alertRegister.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alertRegister, animated: true, completion: nil )
                } else {
                    let alertRegister = UIAlertController(title: "Oops", message: "Create account is complete", preferredStyle: .alert)
                    alertRegister.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alertRegister, animated: true, completion: nil )
                }
            }
        }
    }
    
    @IBAction func buttonDismissPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonGoogleSignupPressed(_ sender: Any) {
        // GIDSignIn.sharedInstance().signIn()
    }
    
}

