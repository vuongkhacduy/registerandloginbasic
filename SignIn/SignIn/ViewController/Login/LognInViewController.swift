//
//  LognInViewController.swift
//  SignIn
//
//  Created by Mojave on 7/26/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

protocol SignInDelegate: class {
    func didSignIn()
}


class LognInViewController: UIViewController {
    

        //Outlet
        @IBOutlet weak var parent_view_mobile_phone: UIView!
        @IBOutlet weak var buttonFacebookLogin: UIButton!
        
        var textfiledEmailAddress = SkyFloatingLabelTextField()
        var textFieldPassword = SkyFloatingLabelTextField()

        
        @IBAction func buttonSignIn(_ sender: Any) {
            Auth.auth().signIn(withEmail: textfiledEmailAddress.text!, password: textFieldPassword.text!) { [weak self] authResult, error in
                guard let strongSelf = self else { return }
                if let error =  error {
                    // alert can not Sign in
                    let alertSignIn = UIAlertController(title: "Oops", message: "Sign In Fail, Please check UserName and Password", preferredStyle: .alert)
                    alertSignIn.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self!.present(alertSignIn, animated: true, completion: nil)
                    print("Can not Sign In")
                } else {
                    // alert complete Sign in
                    let alertSignIn = UIAlertController(title: "Oops", message: "Sign In has been Completed", preferredStyle: .alert)
                    alertSignIn.addAction(UIAlertAction(title: "Ok", style: .default, handler: {action in
//                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
//                        let navigationController = UINavigationController(rootViewController: newViewController)
//                        let scene = UIApplication.shared.connectedScenes.first
//                        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
//                            sd.changeRootView()
                        print("Done")
//                        }
                    }))
                    self!.present(alertSignIn, animated: true, completion: nil)
                    self?.delegate?.didSignIn()
                }
            }
        }
        
        weak var delegate: SignInDelegate?

        override func viewDidLoad() {
            super.viewDidLoad()

            self.view.layoutIfNeeded()
            setupPhoneNumberTextfield()
        }
       
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            let checkAcount = Auth.auth().currentUser
            if checkAcount  == nil{
                print("go Sign In")
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
        //MARK: - Setup layout
        func setupPhoneNumberTextfield(){
             textfiledEmailAddress = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: parent_view_mobile_phone.frame.width, height: parent_view_mobile_phone.frame.height/3))
            textfiledEmailAddress.placeholder = "Email address"
            textfiledEmailAddress.title = "Email address"
            textfiledEmailAddress.selectedTitleColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha: 1.0)
            textfiledEmailAddress.errorColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha:1.0)
            textfiledEmailAddress.keyboardType = .emailAddress
            self.parent_view_mobile_phone.addSubview(textfiledEmailAddress)

            textFieldPassword = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: textfiledEmailAddress.frame.height + textfiledEmailAddress.frame.height*1/2, width: parent_view_mobile_phone.frame.width, height: parent_view_mobile_phone.frame.height/3))
            textFieldPassword.placeholder = "Password"
            textFieldPassword.title = "Password"
            textFieldPassword.selectedTitleColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha: 1.0)
            textfiledEmailAddress.errorColor = UIColor(red: 212/255, green: 10/255, blue: 27/255, alpha:1.0)
            textFieldPassword.keyboardType = .default
            textFieldPassword.isSecureTextEntry = true
            self.parent_view_mobile_phone.addSubview(textFieldPassword)
        }

        
        
        //MARK: IBAction
        @IBAction func buttonRegisterPressed(_ sender: Any) {
            let signUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController")
            self.present(signUpViewController!, animated: true, completion: nil)
        }
        
        @IBAction func buttonGoogleAuthenPressed(_ sender: Any) {
        
        }

        @IBAction func buttonDismissPressed(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
        
        @IBAction func buttonSignInPressed(_ sender: Any) {
            if textfiledEmailAddress.text == "" {
                textfiledEmailAddress.errorMessage = "Enter valid email"
            } else if textFieldPassword.text == ""{
                textFieldPassword.errorMessage = "Password is missing"
            } else {
                
            }
        }
    }






